<?php

/**
 * @file
 * Css autoload configuration page.
 */

/**
 * Page callback mapped to the url /admin/config/development/css-autoload.
 */
function css_autoload_settings_form($form, &$form_state) {

  $form['css_autoload_path'] = array(
    '#title' => t('Path to autoload folder'),
    '#type' => 'textfield',
    '#description' => t('This path is relative to the theme path. The path can be altered per theme by declaring <em>css_autoload_path</em> in the .info-file of your theme.'),
    '#default_value' => variable_get('css_autoload_path', 'css/autoload'),
  );

  return system_settings_form($form);
}
