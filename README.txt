
-- SUMMARY --

CSS Autoload loads css based on what is loaded on the page. Let's say you have
styling for a sidebar, than this styling will only be loaded if that region is
loaded. The module can be enabled/disabled per theme.


-- How does it work? --

CSS Autoload will search for a specific file when something is loaded. For
example, if you enable the module CSS Autoload Node and a node of contenttype
page with viewmode full is loaded, the module will check if the file
node--page--full.autoload.css exists in the css-autoload-folder in your theme.
If so, that file will be added.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configuration in Administration > Development > CSS Autoload settings:

  - Choose on which themes css autoload should be enabled

  - Choose where the autoload files are located in the theme


-- CONTACT --

Current maintainers:
* Martijn Cuppens (puyol) - https://www.drupal.org/user/2416068
